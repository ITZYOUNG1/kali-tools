---
Title: dvwa
Homepage: https://github.com/digininja/DVWA
Repository: https://gitlab.com/kalilinux/packages/dvwa
Architectures: all
Version: 2.0.1+git20220104-0kali4
Metapackages: kali-linux-labs 
Icon: /images/kali-tools-icon-missing.svg
PackagesInfo: |
 ### dvwa
 
  This package contains a PHP/MySQL web application that is damn vulnerable. Its
  main goal is to be an aid for security professionals to test their skills and
  tools in a legal environment, help web developers better understand the
  processes of securing web applications and to aid both students & teachers to
  learn about web application security in a controlled class room environment.
   
  The aim of DVWA is to practice some of the most common web vulnerabilities,
  with various levels of difficulty, with a simple straightforward interface.
  Please note, there are both documented and undocumented vulnerabilities with
  this software. This is intentional. You are encouraged to try and discover as
  many issues as possible.
   
  WARNING: Do not upload it to your hosting provider's public html folder or any
  Internet facing servers, as they will be compromised.
 
 **Installed size:** `5.51 MB`  
 **How to install:** `sudo apt install dvwa`  
 
 {{< spoiler "Dependencies:" >}}
 * adduser
 * apache2
 * libapache2-mod-php
 * mariadb-server
 * nginx
 * php8.1
 * php8.1-fpm
 * php8.1-gd
 * php8.1-mysql
 {{< /spoiler >}}
 
 ##### dvwa-start
 
 
 
 - - -
 
 ##### dvwa-stop
 
 
 
 - - -
 
---
{{% hidden-comment "<!--Do not edit anything above this line-->" %}}
